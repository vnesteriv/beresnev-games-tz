from datetime import datetime
import logging

import redis

from database import session
from models import Report


def create_redis_connect(db_name: int) -> redis.Redis:
    try:
        r = redis.Redis(host='redis', db=db_name, decode_responses=True)
    except Exception as e:
        logging.warning(e)

    return r


def create_metrics_item(p_25: float, median: float, p_75: float):
    timestamp = datetime.now()
    rates = {'timestamp': timestamp, 'p_25': p_25, 'median': median, 'p_75': p_75}
    session.execute(Report.__table__.insert(), rates)
    return


def merge_redis_value(val_col):
    r_stream = create_redis_connect(db_name=0)
    r_history = create_redis_connect(db_name=1)
    pipe = r_history.pipeline()
    pipe.multi()
    for k in val_col:
        r_history.incr(str(k), val_col[k])
        r_stream.delete(str(k))
    pipe.execute()
