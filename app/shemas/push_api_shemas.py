from datetime import datetime

from pydantic import BaseModel


class PushAPI(BaseModel):
    timestamp: datetime
    value: float

