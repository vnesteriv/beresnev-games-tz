import logging

from utils import create_redis_connect
from crud import create_metrics_item, merge_redis_value


logger = logging.getLogger("api")


def write_new_value(value: float) -> None:
    r = create_redis_connect(db_name=0)
    r.incr(str(value))
    return


def get_val_col(db_name: int) -> dict:
    r = create_redis_connect(db_name=db_name)
    s_val = [float(i) for i in r.keys()]
    s_val_c = [int(i) for i in r.mget(r.keys())]
    val_col = dict(zip(s_val, s_val_c))

    return val_col


def search_element(val_col: dict, el: int) -> float:
    cumsum_keys = 0
    for k in sorted(val_col.keys()):
        cumsum_keys += val_col[k]
        if cumsum_keys >= el:
            return k


def prec_calculate(val_col: dict, prec: float) -> float:
    if len(val_col) == 1:
        return list(val_col.keys())[0]
    elif len(val_col) == 0:
        return 0
    count_values = sum(val_col.values())

    np = prec * (count_values + 1) / 100 - 1
    x_np = int(np)
    i_p = np - x_np

    k1 = search_element(val_col=val_col, el=x_np)
    k2 = search_element(val_col=val_col, el=x_np + 1)

    prec = k1 + (k2 - k1) * i_p

    return prec


def calculate_metrics() -> None:
    try:
        val_col_streaming = get_val_col(db_name=0)
        p_25 = prec_calculate(val_col=val_col_streaming, prec=25)
        p_75 = prec_calculate(val_col=val_col_streaming, prec=75)
        if len(val_col_streaming) > 0:
            merge_redis_value(val_col=val_col_streaming)
        val_col_history = get_val_col(db_name=1)
        if len(val_col_history) > 0:
            median = prec_calculate(val_col=val_col_history, prec=50)
        else:
            median = 0
    except Exception as e:
        logger.error(e)

    # p_25 = 0
    # median = 0
    # p_75 = 0
    return p_25, p_75, median


def write_metrics() -> None:
    p_25, p_75, median = calculate_metrics()
    create_metrics_item(p_25=p_25, median=median, p_75=p_75)
    return
