import logging

import redis


def create_redis_connect(db_name: int) -> redis.Redis:
    try:
        r = redis.Redis(host='redis', db=db_name, decode_responses=True)
    except Exception as e:
        logging.warning(e)

    return r



