import os

from clickhouse_sqlalchemy import make_session, get_declarative_base
from sqlalchemy import create_engine, MetaData

DB_HOST = os.environ.get('DB_HOST')
DB_USER = os.environ.get('DB_USER')
DB_PASSWORD = os.environ.get('DB_PASSWORD')
DB_NAME = os.environ.get('DB_NAME')
DB_TABLE = os.environ.get('DB_TABLE')
DB_PORT = os.environ.get('DB_PORT')

# DB_HOST='localhost'
# DB_USER='default'
# DB_PASSWORD=''
# DB_NAME='TZ3'
# DB_TABLE='Report'
# DB_PORT='8123'

uri = f'clickhouse://{DB_USER}:{DB_PASSWORD}@{DB_HOST}:{DB_PORT}'

engine = create_engine(uri)
session = make_session(engine)
metadata = MetaData(bind=engine)
Base = get_declarative_base(metadata=metadata)

