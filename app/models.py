from clickhouse_sqlalchemy import engines
from sqlalchemy import Column, Float, DateTime

from database import Base


class Report(Base):
    __tablename__ = 'report'

    timestamp = Column(DateTime, primary_key=True)
    p_25 = Column(Float)
    median = Column(Float)
    p_75 = Column(Float)

    __table_args__ = (
        engines.Memory(),
    )
