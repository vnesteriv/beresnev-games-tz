import logging
from datetime import datetime

from fastapi import FastAPI
from fastapi_utils.tasks import repeat_every

import models
from database import engine
from shemas.push_api_shemas import PushAPI
from view import write_new_value, write_metrics
import uvicorn


models.Base.metadata.create_all(bind=engine)

app = FastAPI()
logger = logging.getLogger("api")


@app.on_event("startup")
@repeat_every(seconds=5)
def calculate_metrics():
    write_metrics()
    return


@app.post("/new_value", response_model=PushAPI)
async def add_new_value(timestamp: datetime, value: float):
    write_new_value(value=value)
    return

if __name__ == "__main__":
    uvicorn.run(app, host="0.0.0.0", port=8000)