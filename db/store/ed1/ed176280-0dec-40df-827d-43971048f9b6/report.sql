ATTACH TABLE _ UUID 'e02cdee6-969e-4131-bf18-cc4f7c0c1f13'
(
    `timestamp` DateTime,
    `p_25` Float32,
    `median` Float32,
    `p_75` Float32
)
ENGINE = Memory
