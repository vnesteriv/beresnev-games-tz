ATTACH TABLE _ UUID '6f0bf5cc-2d62-4cf0-a2a6-3275e20aed67'
(
    `id` String,
    `created_at` DateTime,
    `revenue` Float32
)
ENGINE = MergeTree
ORDER BY created_at
SETTINGS index_granularity = 8192
