ATTACH TABLE _ UUID '2d404079-df48-46e5-83b5-02c19ceab9c3'
(
    `created_at` DateTime,
    `id` String,
    `campaign_name` String,
    `country` String
)
ENGINE = MergeTree
ORDER BY created_at
SETTINGS index_granularity = 8192
