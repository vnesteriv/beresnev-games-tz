ATTACH TABLE _ UUID '5dd388ea-70c7-4d2c-82b9-3ebb6b2dc584'
(
    `id` String,
    `created_at` DateTime,
    `parameters` String
)
ENGINE = MergeTree
ORDER BY created_at
SETTINGS index_granularity = 8192
