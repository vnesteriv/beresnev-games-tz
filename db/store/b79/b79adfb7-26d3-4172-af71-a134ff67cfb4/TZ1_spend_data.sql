ATTACH TABLE _ UUID '56b59ead-95cc-489c-845c-7d6b4ae84776'
(
    `date` Date,
    `campaign_name` String,
    `country` String,
    `spend` Float32
)
ENGINE = MergeTree
ORDER BY date
SETTINGS index_granularity = 8192
