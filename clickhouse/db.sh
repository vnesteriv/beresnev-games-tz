#!/bin/sh

clickhouse-client -q "CREATE DATABASE IF NOT EXISTS Beresnev_Games"
clickhouse-client -q "CREATE TABLE IF NOT EXISTS Beresnev_Games.TZ1_install_data (created_at DateTime, id String, campaign_name String, country String) ENGINE = MergeTree() ORDER BY (created_at)"
cat TZ1_install_data.tsv | sed 1,1d | clickhouse-client --query "INSERT INTO Beresnev_Games.TZ1_install_data FORMAT TSV" --max_insert_block_size=100000

clickhouse-client -q "CREATE TABLE IF NOT EXISTS Beresnev_Games.TZ1_spend_data (date Date, campaign_name String, country String, spend Float) ENGINE = MergeTree() ORDER BY (date)"
cat TZ1_spend_data.tsv | sed 1,1d | clickhouse-client --query "INSERT INTO Beresnev_Games.TZ1_spend_data FORMAT TSV" --max_insert_block_size=100000

clickhouse-client -q "CREATE TABLE IF NOT EXISTS Beresnev_Games.TZ2_iap_parameters (id String, created_at DateTime, parameters String) ENGINE = MergeTree() ORDER BY (created_at)"
cat TZ2_iap_parameters.tsv | sed 1,1d | clickhouse-client --query "INSERT INTO Beresnev_Games.TZ2_iap_parameters FORMAT TSV" --max_insert_block_size=100000

clickhouse-client -q "CREATE TABLE IF NOT EXISTS Beresnev_Games.TZ2_lost_revenue (id String, created_at DateTime, revenue Float) ENGINE = MergeTree() ORDER BY (created_at)"
cat TZ2_lost_revenue.tsv | sed 1,1d | clickhouse-client --query "INSERT INTO Beresnev_Games.TZ2_lost_revenue FORMAT TSV" --max_insert_block_size=100000




