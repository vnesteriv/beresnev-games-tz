SELECT r.id,
       r.created_at as r_c,
       r.revenue,
       p.parameters
FROM Beresnev_Games.TZ2_lost_revenue as r
         LEFT JOIN Beresnev_Games.TZ2_iap_parameters as p
                   ON p.id = r.id AND toStartOfHour(r.created_at) = toStartOfHour(p.created_at)
WHERE r.created_at BETWEEN p.created_at AND DATEADD(second, (SELECT quantile(0.9) (second_diff)
                                                             FROM (SELECT dateDiff(second, p.created_at, r.created_at) as second_diff
                                                                   FROM Beresnev_Games.TZ2_lost_revenue as r
                                                                            INNER JOIN Beresnev_Games.TZ2_iap_parameters as p
                                                                                       ON p.id = r.id AND
                                                                                          toStartOfHour(r.created_at) =
                                                                                          toStartOfHour(p.created_at)
                                                                   WHERE p.created_at <= r.created_at) as r_t),
                                                            p.created_at);