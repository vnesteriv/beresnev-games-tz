SELECT date, campaign_name, country, count (id) as installs, sum (spend) as spends
FROM (
    (SELECT DATE (created_at) as date,
    left (campaign_name, locate(campaign_name, ' ') - 1) as campaign_name,
    ucase(country) as country,
    id,
    NULL as spend
    FROM Beresnev_Games.TZ1_install_data AS i)
    UNION ALL
    (SELECT DATE (date) as date, campaign_name, country, NULL as id, spend
    FROM Beresnev_Games.TZ1_spend_data)) as ut
GROUP BY date, campaign_name, country;