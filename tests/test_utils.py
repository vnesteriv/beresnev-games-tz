import pytest

from app.view import search_element, prec_calculate


@pytest.mark.parametrize('val_col,el,k', [({0: 2, 1: 2}, 3, 1),
                                          ({5: 1}, 1, 5)])
def test_search_element(val_col: dict, el: int, k: float):
    assert search_element(val_col=val_col, el=el) == k


@pytest.mark.parametrize('val_col,prec,prec_value', [({0: 2, 1: 2, 3: 1, 4: 1, 3: 4}, 50, 0.8),
                                                     ({0: 2, 1: 2, 3: 1, 4: 1, 3: 4}, 25, 3.0),
                                                     ({0: 2, 1: 2, 3: 1, 4: 1, 3: 4}, 75, 3.0)])
def test_prec_calculate(val_col, prec, prec_value):
    assert prec_calculate(val_col=val_col, prec=prec) == prec_value
